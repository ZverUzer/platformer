﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour{
	public Text scoreText;
	public Text maxScore;

	void Start(){
		if (scoreText != null) {
			scoreText.text = string.Concat ("Счёт: ", Global.score);
		}
		if (maxScore != null) {
			if (Global.score > PlayerPrefs.GetInt ("HigsScore")) {
				PlayerPrefs.SetInt ("HighScore", Global.score);
			}
			maxScore.text = string.Concat("Рекорд: ", PlayerPrefs.GetInt("HighScore"));
		}
	}

	public void NewGameClick(){
		Global.SetDefaults ();
		SceneManager.LoadScene (2);
	}

	public void ExitClick(){
		Application.Quit ();
	}

	public void MenuClick(){
		SceneManager.LoadScene (0);
	}
}
