﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour {
	public GameObject bullet;
	public Vector2 velocity;
	public float direction = -1.0f;

	public Vector2 bulletOffset = new Vector2(1.0f, 0.6f);
	public float coolDown = 1.0f;

	void Start(){
		StartCoroutine ("Shooting");
	}

	IEnumerator Shooting(){
		for(;;) {
			GameObject go = (GameObject)Instantiate (bullet, ((Vector2)transform.position + bulletOffset), Quaternion.identity);
			go.GetComponent<Rigidbody2D> ().velocity = new Vector2 (velocity.x*direction, velocity.y);

			yield return new WaitForSeconds (coolDown);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		Debug.Log (other.tag);
		if(other.tag.Equals("PlayerBullet")){
			Destroy(gameObject);
		}
	}
}
