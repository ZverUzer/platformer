﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag.Equals ("Ground")) {
			Destroy (gameObject);
		}
		if (this.tag.Equals ("PlayerBullet") && other.tag.Equals ("Enemy")) {
			Destroy (gameObject);
		}
	}
}
