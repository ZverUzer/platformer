﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CnControls;

public class BatyaScript : MonoBehaviour
{
    public float speed = 3.0f;
	public float jumpForce = 5.0f;

	public Transform groundCheck;
	public LayerMask groundLayer;
	public Text livesCounterText;
	public Text scoreText;

    private Rigidbody2D rb;
	private SpriteRenderer sprite;
	private bool isGrounded = false;
	private bool isTriggerEntered = false;
	public bool lookingTorwards = false;

	void Start(){		
		UpdateLives ();
		UpdateScores ();
	}

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
		CheckGround();
		if (CnInputManager.GetAxis ("Horizontal")!=0) {
			Run ();
		}

		if (CnInputManager.GetButton("Jump") && isGrounded)
		{
			Jump();
		}
    }

    private void CheckGround() {
		isGrounded = Physics2D.OverlapCircle (groundCheck.position, 0.12f, groundLayer);
    }

    private void Run()
    {
		Vector3 direction = transform.right* CnInputManager.GetAxis("Horizontal"); 

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);

        sprite.flipX = direction.x > 0;
		lookingTorwards = direction.x > 0;
    }

    private void Jump()
    {
		rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }

	private void UpdateLives(){
		livesCounterText.text = string.Concat ("Жизни: ", Global.lives);
	}

	private void UpdateScores(){
		scoreText.text = string.Concat ("Счёт: ", Global.score);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag.Equals("Enemy") && !isTriggerEntered) {
			isTriggerEntered = true;
			Global.lives--;
			if (Global.lives < 0) {
				SceneManager.LoadScene (1);
			} else {				
				SceneManager.LoadScene (Global.levelNum);
			}
		}
		if (other.tag.Equals ("Star")) {
			Global.score++;
			UpdateScores ();
			other.gameObject.SetActive (false);
		}
		if (other.tag.Equals ("Portal") && !isTriggerEntered) {
			isTriggerEntered = true;
			Global.levelNum++;
			SceneManager.LoadScene (Global.levelNum);
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (isTriggerEntered) {
			isTriggerEntered = false;
		}
	}
}
