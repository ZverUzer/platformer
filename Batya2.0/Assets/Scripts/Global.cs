﻿using UnityEngine;

public class Global  {
	public static int levelNum = 1;
	public static int lives = 3;
	public static int score = 0;

	public static void SetDefaults(){
		levelNum = 2;
		lives = 3;
		score = 0;
	}
}
