﻿using UnityEngine;
using CnControls;
using System.Collections;

public class ShootingScript : MonoBehaviour {
	public GameObject bullet;
	public Vector2 velocity = new Vector2(5.0f, 0f);

	public Vector2 bulletOffset = new Vector2(0.4f, 0.1f);
	public float coolDown = 1.0f;

	private bool canShoot = true;
	private BatyaScript batyaScript;

	void Start(){
		GameObject batya = GameObject.Find ("batya");
		batyaScript = batya.GetComponent<BatyaScript> ();		
	}

	void FixedUpdate () {
		if(CnInputManager.GetButtonDown("Fire2") && canShoot){
			float batyaScale = (batyaScript.lookingTorwards) ? 1f : -1f;

			GameObject go = (GameObject)Instantiate (bullet, (Vector2)transform.position + bulletOffset * batyaScale, Quaternion.identity);
			go.GetComponent<Rigidbody2D> ().velocity = new Vector2 (velocity.x*batyaScale, velocity.y);
		}
	}
}